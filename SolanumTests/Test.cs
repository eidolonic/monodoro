﻿using Moq;
using NUnit.Framework;
using Solanum;
using System;
using System.Timers;

namespace SolanumTests
{
	[TestFixture ()]
	public class Test
	{
		[Test ()]
		public void NewTomatoDefaultsToPomodoroCultivar ()
		{
			var p = new Tomato ();

			Assert.That (p.GetCultivar (), Is.EqualTo (Cultivar.Pomodoro));
		}

		[Test ()]
		public void PomodoroLifespanDefaultsToTwentyFiveMinutes ()
		{
			var p = new Tomato ();

			Assert.That (p.GetLifespan, Is.EqualTo (25));
		}

		[Test ()]
		public void ShortBreakLifespanDefaultsToFiveMinutes ()
		{
			var p = new Tomato (Cultivar.ShortBreak);

			Assert.That (p.GetLifespan, Is.EqualTo (5));
		}

		[Test ()]
		public void LongBreakLifespanDefaultsToFifteenMinutes ()
		{
			var p = new Tomato (Cultivar.LongBreak);

			Assert.That (p.GetLifespan, Is.EqualTo (15));
		}

		[Test ()]
		public void DisposingTimerMakesNotEnabled ()
		{
			Timer timer = new Timer (1000);
			timer.Start ();
			timer.Stop ();
			timer.Dispose ();

			Assert.That (!timer.Enabled);
		}

		[Test ()]
		public void SchedulerStartsTomatoTimer()
		{
			var scheduler = new Mock<IScheduler> ();
			var writer = new Mock<IWriter> ();
			new Implementation (scheduler.Object, writer.Object);

			scheduler.Raise (s => s.TomatoStarts += null, EventArgs.Empty);

			Assert.That (scheduler.Object.TimerIsAvailable, Is.EqualTo(false));
			writer.Verify (w => w.Write ("Tomato starts."));
		}

		[Test ()]
		public void SchedulerStopsTomatoTimer ()
		{
			var scheduler = new Mock<IScheduler> ();
			var writer = new Mock<IWriter> ();
			var implementation = new Implementation (scheduler.Object, writer.Object);

			scheduler.Raise (s => s.TomatoStops += null, EventArgs.Empty);

			Assert.That (implementation.TomatoStopsWasRaised, Is.EqualTo (true));
			writer.Verify (w => w.Write ("Tomato stops."));
		}

		[Test ()]
		public void WriteOnTimerExpired ()
		{
			Mock<IScheduler> scheduler = new Mock<IScheduler> ();
			Mock<IWriter> writer = new Mock<IWriter> ();
			new Implementation (scheduler.Object, writer.Object);

			scheduler.Raise (s => s.TimerExpires += null, EventArgs.Empty);

			writer.Verify (w => w.Write ("Timer expired."));
		}
	}
}

