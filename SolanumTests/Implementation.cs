﻿using Solanum;
using System;

namespace SolanumTests
{
	public class Implementation
	{
		public bool TomatoStopsWasRaised;
		private readonly IScheduler _scheduler;
		private readonly IWriter _writer;

		public Implementation (IScheduler s, IWriter w)
		{
			_scheduler = s;
			_writer = w;

			_scheduler.TimerExpires += TimerExpires;
			_scheduler.TomatoStarts += TomatoStarts;
			_scheduler.TomatoStops += TomatoStops;
			_scheduler.TomatoStops += (object sender, EventArgs e) => {
				TomatoStopsWasRaised = true;
			};
		}

		private void TimerExpires (object sender, EventArgs e)
		{
			_writer.Write ("Timer expired.");
		}

		private void TomatoStarts (object sender, EventArgs e)
		{
			_writer.Write ("Tomato starts.");
		}

		private void TomatoStops (object sender, EventArgs e)
		{
			_writer.Write ("Tomato stops.");
		}
	}
}

