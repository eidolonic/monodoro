﻿using System;

namespace Solanum
{
	public interface IScheduler
	{
		Tomato GetTomato { get; }
		event EventHandler TimerExpires;
		event EventHandler TomatoStops;
		event EventHandler TomatoStarts;
		bool TimerIsAvailable ();
		void BeginPomodoro (Cultivar cultivar);
		void EndPomodoro ();
	}
}