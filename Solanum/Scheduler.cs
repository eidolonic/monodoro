﻿using System;
using System.Timers;

namespace Solanum
{
	public class Scheduler : IScheduler
	{
		public event EventHandler TimerExpires;
		public event EventHandler TomatoStops;
		public event EventHandler TomatoStarts;
		private Tomato _tomato;
		private Timer _timer;
		private int _timerElapsedCount;

		public Scheduler ()
		{
		}

		public Tomato GetTomato {
			get {
				return _tomato;
			}
		}

		public bool TimerIsAvailable ()
		{
			return !_timer.Enabled;
		}

		public void BeginPomodoro (Cultivar cultivar)
		{
			InitTimer ();
			_tomato = new Tomato (cultivar);
			_timer.Start ();

			OnTomatoStarts ();
		}

		public void EndPomodoro ()
		{
			if (_timer == null)
				return;

			DisposeTimer ();

			OnTomatoStops ();
		}

		protected virtual void OnTimerExpires ()
		{
			var e = TimerExpires;
			if (e != null)
				e (this, EventArgs.Empty);
		}

		protected virtual void OnTomatoStarts ()
		{
			var e = TomatoStarts;
			if (e != null)
				e (this, EventArgs.Empty);
		}

		protected virtual void OnTomatoStops ()
		{
			var e = TomatoStops;
			if (e != null)
				e (this, EventArgs.Empty);
		}

		private void OnTimerElapsed () 
		{
			_timerElapsedCount++;
			if (_timerElapsedCount >= _tomato.GetLifespan () * 60000)
				OnTimerExpires ();
		}

		private void InitTimer ()
		{
			_timer = new Timer (1000);
			_timer.Elapsed += (object sender, ElapsedEventArgs e) => {
				OnTimerElapsed ();
			};
		}

		private void DisposeTimer ()
		{
			_timer = null;
		}
	}
}

