﻿using System;

namespace Solanum
{
	public interface IWriter
	{
		void Write (string message);
	}
}

